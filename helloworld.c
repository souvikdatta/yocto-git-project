#include <stdio.h>
#include "util.h"

int main()
{
    int num1 = 2, num2 = 3;
    printf ("The sum of two numbers - %d and %d are: %d\n", num1, num2, sum (num1, num2));
    printf ("This is a new line that has been added\n");
    printf ("This is a new line that has been added to show more modification\n");
    return 0;
}
